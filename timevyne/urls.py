from django.contrib import admin
from django.urls import path
from timeline.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('timeline/', timeline, name='timeline'),
    path('register/', register, name='register'),
    path('login/', login, name='login'),
    path('profile/', profile, name='login'),
]
