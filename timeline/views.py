from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm
from django.contrib.auth import authenticate, login
from .context import header

def index(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'home.html')

def register(request):
    # if there is a POST request the lets register  the user
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            # Process the form data
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # Create a new user
            user = User.objects.create_user(username=username, email=email, password=password)

            # Save the user object
            user.save()

            # Redirect to the login page
            return redirect('login')
    else:
        form = RegistrationForm()

    return render(request, 'register.html', { 'form': form })

def login_user(request):
    if request.method == 'POST':
       form = LoginForm(request.POST)
       if form.is_valid():
           user = form.cleaned_data['username']
           password = form.cleaned_data['password']
           user = authenticate(username=user, password=password)
           if user is not None:
               login(request, user) 
               return redirect('home')
           else:
               return redirect('login')
    else:
        form = LoginForm()

    return render(request, 'login.html', { 'form': form })

def profile(request):
    return render(request, 'profile.html')

def timeline(request):
    return render(request, 'home.html')
