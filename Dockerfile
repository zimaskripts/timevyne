FROM python:3.11

# Set the working directory to /src
WORKDIR /src

# Copy the requirements.txt file to the container
COPY requirements.txt /src/requirements.txt

# Install the project dependencies
RUN pip install -r requirements.txt

# Copy the rest of the project files to the container
COPY . /app

# Run the Django development server
CMD python manage.py livereload
CMD python manage.py runserver 0.0.0.0:8000
